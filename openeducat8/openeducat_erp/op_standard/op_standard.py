# -*- coding: utf-8 -*-
###############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech-Receptives(<http://www.techreceptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from openerp import models, fields
from openerp import models, fields, api, exceptions, _


import logging
_logger = logging.getLogger(__name__)


class OpStandard(models.Model):
    _name = 'op.standard'
    _order = 'sequence'


    @api.one
    @api.onchange('course_id')
    def _get_subjects(self):
        res = []
        for sid in self.course_id.subject_ids:
            res.append(sid)
            _logger.info(sid)
        _logger.info(res)
        self.course_subjects = res
        return res

    code = fields.Char('Code', size=128, required=True)
    name = fields.Char('Name', size=32, required=True)
    course_id = fields.Many2one('op.course', 'Course', required=True)
    payment_term = fields.Many2one('account.payment.term', 'Payment Term')
    sequence = fields.Integer('Sequence')
    division_ids = fields.Many2many('op.division', string='Groups')
    student_ids = fields.Many2many('op.student', string='Student(s)')
    course_subjects = fields.Char(string="Course subject",compute=_get_subjects)
    subject_ids = fields.Many2many('op.subject',string='Subjects',domain=_get_subjects)#related="course_id.subject_ids",string='Subjects')
#     class_ids = fields.Many2many('op.gr.setup', string='Class')

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
