# -*- coding: utf-8 -*-
###############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech-Receptives(<http://www.techreceptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from openerp import models, fields, api
from openerp import SUPERUSER_ID


class OpCourse(models.Model):
    _name = 'op.course'

    name = fields.Char('Name', size=128, required=True)
    code = fields.Char('Code', size=128, required=True)
    sequence_id = fields.Many2one(string="Sequence", comodel_name="ir.sequence")
    #section = fields.Char('Section', size=32)
    evaluation_type = fields.Selection(
        [('normal', 'Normal'), ('GPA', 'GPA'), ('CWA', 'CWA'), ('CCE', 'CCE')],
        'Evaluation Type')
    payment_term = fields.Many2one('account.payment.term', 'Payment Term')
    subject_ids = fields.Many2many('op.subject', string='Subject(s)')

    @api.model
    def create_sequence(self, vals):
        # in account.journal code is actually the prefix of the sequence
        # whereas ir.sequence code is a key to lookup global sequences.
        prefix = vals['code'].upper()

        seq = {
            'name': vals['name'],
            'implementation':'no_gap',
            'prefix': "%(year)s/" + prefix,
            'padding': 5,
            'number_increment': 1
        }
        if 'company_id' in vals:
            seq['company_id'] = vals['company_id']
        return self.env['ir.sequence'].sudo().create(seq)
    
    @api.model
    def create(self, vals):
        if not 'sequence_id' in vals or not vals['sequence_id']:
            vals.update({'sequence_id': self.sudo().create_sequence(vals).id})
        return super(OpCourse, self).create(vals)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
