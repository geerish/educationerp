# -*- coding: utf-8 -*-
###############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech-Receptives(<http://www.techreceptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from openerp import models, fields, api, _


class StudentMigrate(models.TransientModel):

    """ Student Migration Wizard """
    _name = 'student.migrate'

    date = fields.Date('Date', required=True, default=fields.Date.today())
    from_batch_id = fields.Many2one(string="From Batch", comodel_name="op.batch")
    to_batch_id = fields.Many2one(string="To Batch", comodel_name="op.batch")
    from_course_id = fields.Many2one(string="From Course", comodel_name="op.course")
    to_course_id = fields.Many2one(string="To Course", comodel_name="op.course")       
    from_standard_id = fields.Many2one('op.standard', 'From Semester') 
    to_standard_id = fields.Many2one('op.standard', 'To Semester')
    student_ids = fields.Many2many('op.student', string='Student(s)', required=True)

    # _sql_constraints = [
    #     ('from_to_standard_check', 'check(from_standard_id != to_standard_id)',
    #      _("From Student must not be same as To Student !")),
    # ]

    @api.one
    def go_forward(self):
#         activity_type = self.env["op.activity.type"]
        student_obj = self.env['op.student']
        if self.from_standard_id:
            if self.to_standard_id:
                to_update = []
                if self.student_ids:
                    for student in self.student_ids:
                        student.write({'standard_id':self.to_standard_id.id}) 
                else:
                    for student in self.from_standard_id.student_ids:
                        student.write({'standard_id':self.to_standard_id.id})       

        if self.from_course_id:
            if self.to_course_id:
                to_update = []
                if self.student_ids:
                    for student in self.student_ids:
                        student.write({'course_id':self.to_course_id.id})  
                else:
                    for student in self.from_course_id.student_ids:
                        student.write({'course_id':self.to_course_id.id} )      


        if self.from_batch_id:
            if self.to_batch_id:
                to_update = []
                if self.student_ids:
                    for student in self.student_ids:
                        student.write({'batch_id':self.to_batch_id.id}) 
                else:
                    for student in self.from_batch_id.student_ids:
                        student.write({'batch_id':self.to_batch_id.id})       
                     


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
